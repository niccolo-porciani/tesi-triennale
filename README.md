[![build status](https://gitlab.com/niccolo-porciani/tesi-triennale/badges/master/pipeline.svg)](https://gitlab.com/niccolo-porciani/tesi-triennale/-/pipelines)

Sono disponibili:
- Le [slides](https://gitlab.com/niccolo-porciani/tesi-triennale/-/jobs/artifacts/master/raw/slides.pdf?job=compile_slides)
- Il [long abstract](https://gitlab.com/niccolo-porciani/tesi-triennale/-/jobs/artifacts/master/raw/long_abstract.pdf?job=compile_abstract)
