# Slide 1 (~1 min)
- Identità, contesto, equilibrazione finale
- Definizione di processo e origine della stocasticità
- Ben definizione della variazione di energia libera

# Slide 2 (~2.5 min, fino a 3.5)
## Dimostrazione classica isolata
    - Nel canonico
    - Punto chiave, da dire bene: la media andrebbe fatta con una qualche misura sullo spazio delle traiettorie, ma il fatto che il sistema sia isolato e quindi la dinamica deterministica permette di ricondurre alla distribuzione delle condizioni iniziali. Si può fare anche con Liouville
    - Altro punto importante: per traiettorie classiche si verifica che il lavoro è la variazione di Hamiltoniana
    - Conto: un pezzo semplifica la misura, l'altro costruisce Zf
## Dimostrazione classica coupled
    - Dire bene il contesto
    - Punto chiave: indipendenza dalla velocità (stesso punto iniziale e finale)
    - Se fanno storie come argomento alternativo piccolezza del coupling e semplifichi.

# Slide 3 (~2 min, fino a 5.5)
- Accenno: utilità teorica essendo arbitrariamente fuori equilibrio, utilità sperimentale
- Violazioni locali: secondo principio da Jensen, violazioni esponenzialmente soppresse
- Fluttuazione dissipazione: accenno di dimostrazione, enunciato

# Slide 4 (~1 min, fino a 7)
- Idea e argomento intuitivo
- Definizione del processo reverse
- Risultato centrale, accennato ma detto bene
- Stima della probabilità di osservare un dominante

# Slide 5 (~3 min, fino a 10)
- Setup sperimentale
- Switching rate diverse -> rev/irrev (+ verifica che lo siano)
- Problemi con il caso reversibile (?)
- Torna nel caso irreversibile, confronto con FD

# Timestamps:
- 1
- 3.5
- 5.5
- 7
- 10

