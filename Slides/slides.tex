\def\jobname{\detokenize{slides}}

\documentclass{beamer}
% Configurazione Beamer
\input{beamerconfig.sty}

\usepackage{style}

\usepackage[
    backend=biber
    ,style=authoryear
    ,dashed=false
    ,autocite=inline
    ,url=false
    ,doi=false
    ,eprint=false
    ,sorting=none
    ]{biblatex}
\setbeamertemplate{bibliography item}[text]
\addbibresource{niccoloTesiTriennale.bib}
\AtEveryBibitem{\clearfield{pages}}
\renewcommand*{\bibfont}{\scriptsize}

\newcommand{\boldtitle}[1]{\textbf{\textcolor{UniPi}{#1}}}

\usepackage{tikz}

% Pagina del titolo
\input{titlepage.tex}

% Metadati
\author{Niccolò Porciani}
\title{L'Identità di Jarzynski}
\institute{Università di Pisa}
\date{20 Ottobre 2020}

\begin{document}

{% Pagina iniziale
% Sfondo
\usebackgroundtemplate{
    \adjustbox{width=0.5\paperwidth, raise=-9cm, right=12.7cm}{
        \transparent{0.1}
        \includegraphics{./staticimg/cherubino_pant541.eps}
    }
}
\frame{\titlepage}
}

\begin{frame}
    \frametitle{L'identità di Jarzynski}
    \cite{Jarzynski0}, identità valida per un sistema termodinamico a contatto con un bagno termico, sottoposto a un generale processo anche irreversibile e lasciato poi rilassare all'equilibrio con il bagno
    \begin{equation}\label{eq:Jarzynski}
        \avg{e^{-\beta W}} = e^{-\beta \Delta F}
    \end{equation}%
    \begin{itemize}
        \item $ \Delta F $ tra stato iniziale e finale, entrambi di equilibrio
        \item $ W $ lavoro compiuto sul sistema durante il processo
        \item $ \avg{\placeholderdot} $ media statistica sulle possibili realizzazioni del processo
    \end{itemize}
    Processo definito attraverso un'Hamiltoniana parametrica $ H(\lambda(t)) $ con evoluzione definita, indeterminazione statistica proveniente dal contatto con il bagno termico.
\end{frame}

\begin{frame}
    \setlength{\leftmargini}{0.5cm}
    \frametitle{Ipotesi e dimostrazione}
    \begin{itemize}
        \item<1-> Sistema classico con evoluzione isolata
            \begin{itemize}
                \item $ \mathcal{W}[\z(t)] = \int_{t_i}^{t_f} \od{\lambda}{t} \od{H}{\lambda}(\z(t),\lambda(t)) \dif{t} $
                \item Sistema isolato $ \!\implies\! $ traiettorie classiche, $ \mathbb{P}[\z(t)] = \Z_i^{-1} e^{-\beta H(\z_i,\lambda_i)} $
                \item Per le traiettorie classiche $ \mathcal{W} = \sbr{H\!\del{\z(t),\lambda(t)}}_{t_f}^{t_i} $
                \item Il termine in $ t_i $ si semplifica con la misura, quello in $ t_f $ ricostruisce $ \Z_f $
                \item $ \avg{e^{-\beta W}} = \frac{\Z_f}{\Z_i} = e^{-\beta\Delta F} $
                \item Lo stato finale è già di equilibrio
            \end{itemize}
        \item<2-> Estensione: sistema classico non isolato
            \begin{itemize}
                \item Hamiltoniana complessiva $ \mathcal{G}_\lambda = H_\lambda + \mathcal{H} + h $, funzione di partizione $ \mathcal{Y}_\lambda $
                \item Assumiamo coupling debole ed ergodicità
                \item Distribuzione iniziale complessiva con Boltzmann
                \item Dal risultato precedente $ \avg{e^{-\beta W}} = \mathcal{Y}_f / \mathcal{Y}_i $
                \item RHS indipendente dalla velocità, LHS pari a $ e^{-\beta\Delta F} $ per processo sufficientemente lento
            \end{itemize}
        \item<3-> Altre possibilità: caso quantistico, validità per dinamica stocastica e simulazioni numeriche (es. Metropolis Hastings)
    \end{itemize}
\end{frame}

\begin{frame}
    \uncover<1->{
    \frametitle{Applicazioni}
    \boldtitle{Violazioni locali del secondo principio}
    Da Jensen otteniamo il Secondo Principio, $ \avg{W} \geq \Delta F $. \cite{JarzynskiReview}: probabilità di violazione locale
    \begin{align*}
    \prob\!\sbr{W \leq \Delta F - \zeta}
    &= \int_{-\infty}^{\Delta F - \zeta} \dif{\prob(W)}\,
    \alert{\leq} \int_{-\infty}^{\Delta F - \zeta} e^{\beta\del{\Delta F - \zeta - W}} \dif{\prob(W)}\\[1ex]
    &= e^{-\beta \zeta} e^{\beta \Delta F} \avg{e^{-\beta W}}
    = e^{-\beta \zeta}
    \end{align*}
    }\uncover<2->{%
    \boldtitle{Teorema di Fluttuazione Dissipazione} Espandiamo il logaritmo della media in cumulanti e assumiamo una statistica Gaussiana
    \[
    \ln\avg{e^{-\beta W}} = \sum_{n=1}^{\infty} \del{-\beta}^n \frac{\kappa_n}{n!} = - \beta \avg{W} + \beta^2 \frac{\avg{W^2} - \avg{W}^2}{2}
    \]
    otteniamo
    \[
    \avg{W_\mathrm{diss}} = \avg{W} - \Delta F = \beta \frac{\avg{\delta W}^2}{2}
    \]
    }
\end{frame}

\begin{frame}
    \setlength{\leftmargini}{0.5cm}
    \frametitle{Eventi rari e verificabilità sperimentale}
    \begin{itemize}
    \item<1-> $ {\prob[\vec{z}(t)]} $ avrà un picco sugli eventi ``tipici'', ma il fattore esponenziale $ e^{-\beta W[\vec{z}(t)]} $ lo sposta verso valori più bassi del lavoro, che saranno ``dominanti'' nella media.
    \item<2-> Più precisamente (\cite{rareevents}): dato processo ``forward'' $ \lambda = \lambda^F(t)$, $ t\in[0,\tau] $ ed il corrispondente processo ``reverse''
    \begin{equation*}
    \lambda = \lambda^R(t) = \lambda^F(\tau - t)\qquad t \in [0,\tau]
    \end{equation*}
    Allora la probabilità relativa $ \prob^R(\mathcal{S}^R) $ di un insieme di realizzazioni $ \mathcal{S}^R $ del processo reverse è pari al peso relativo in $ \avg{e^{-\beta W}}_F $ delle traiettorie $ \mathcal{S}^F $ ottenute per time reversal di $ \mathcal{S}^R $.\\
    
    
%    se per la dinamica vale il teorema di Crooks
%    \begin{equation}\label{eq:Crooks}
%    \frac{\prob^F [\gamma^F]}{\prob^R [\gamma^R]} = \exp\!\del{\beta W^F_\mathrm{diss}[\gamma^F]} = \exp\!\del{-\beta W^R_\mathrm{diss}[\gamma^R]}
%    \end{equation}
    %    \begin{equation}
    %        \prob[\mathcal{S}^F] = {\avg{e^{-\beta W^R} \vert \mathcal{S}^R}}\Big/{\avg{e^{-\beta W^R}}}
    %    \end{equation}
    \item<3-> Si ottiene che la probabilità di osservare almeno una traiettoria ``dominante'' per il processo forward è approssimativamente
    \begin{equation}
    P_\mathrm{dom} \sim e^{-\beta \avg{W_\mathrm{diss}^R}}
    \end{equation}
    $ \implies $ sperimentalmente fattibile solo se $ W_\mathrm{diss} \sim k_B T $
    \end{itemize}
\end{frame}

\begin{frame}[squeeze]
    \frametitle{Verifiche sperimentali}
    \cite{experimentalRNA}, verifica sperimentale con dispiegamento meccanico di RNA, misurando forza e allungamento. Parametro di controllo $ \lambda = F_\mathrm{ext} $.
    \scalebox{0.9}{\begin{minipage}{1.1\textwidth}\vspace{0.1cm}
    \begin{itemize}
        \item Variando $ \dot{\lambda} $, $ 2-5\ \mathrm{pN\, s^{-1}} \to $ è reversibile, $ 34,52\ \mathrm{pN\, s^{-1}} $ irreversibili
        \item $ N = 280 $ per switching rate, $ W_\text{irr} \lesssim 3 k_B T \implies P_\mathrm{dom} \sim \frac{1}{20} $
        \item Criterio: $ \Delta F_\mathrm{JE} $ coincidono tra loro e con $ \Delta F_A = \avg{W_\mathrm{rev}} $ entro $ k_B T /2 $
        \item Nel caso reversibile $ \Delta F_\mathrm{FD} $ e $ \Delta F_\mathrm{JE} $ falliscono per rumore a bassa frequenza
        \item Nei caso irreversibili $ \Delta F_\mathrm{FD} $ fallisce, $ \Delta F_\mathrm{JE} $ è corretto entro incertezza
    \end{itemize}\vspace{0.1cm}
    \end{minipage}}
    
    % FIGURE
    \begin{tikzpicture}
        \colorlet{textColor}{UniPi!60};
        \node(a){\includegraphics[height=3.5cm,frame]{./staticimg/Liphardt_setup.png}};
        \node(b) at (a.south east)[
            anchor=center,
            xshift=0.7cm,
            yshift=1cm]{
                \includegraphics[height=3.5cm,frame]{./staticimg/Liphardt_force_extension.png}
        };
        \node at (a.north east)[
            anchor=north west,
            xshift=-2mm,
            align=left,
            text=textColor,
            font=\tiny,
            ]{
                \baselineskip=2pt
                \textcolor{UniPi}{$ \blacktriangleleft $}
                \textcolor{textColor}{Setup}\\
                \hphantom{$ \blacktriangleleft $ }\textcolor{textColor}{sperimentale}
                \par
        };
        \node at (b.south west)[
        anchor=south east,
        xshift=+2mm,
        align=right,
        font=\tiny,
        text=UniPi!60
        ]{
            \baselineskip=2pt
            Curve forza - estensione\hphantom{$ \blacktriangleleft $ }\\
            nel caso reversibile\hphantom{$ \blacktriangleleft $ }\\
            e irreversibile \textcolor{UniPi}{$ \blacktriangleright $}
            \par
        };
    \end{tikzpicture}%
    \hfill
    \includegraphics[height=4.5cm,raise=0.1cm]{./staticimg/Liphardt_curves.png}
\end{frame}

{% Pagina finale
    % Sfondo
    \usebackgroundtemplate{
        \adjustbox{width=0.5\paperwidth, raise=-9cm, right=12.7cm}{
            \transparent{0.05}
            \includegraphics{./staticimg/cherubino_pant541.eps}
        }
    }
\begin{frame}
    \frametitle{Ringraziamenti e Bibliografia}
    Vorrei ringraziare il mio relatore, Claudio Bonati, per la disponibilità ed i consigli che mi ha fornito, la mia famiglia, che mi ha sostenuto nel mio percorso, ed i miei amici, con i quali ho condiviso questo stesso percorso.
    \begin{center}
        \textcolor{UniPi}{---\adjustbox{raise=.2pt}{$\boldsymbol{\cdot}$}---}
    \end{center}
%    \begin{center}
%        \boldtitle{Bibliografia}
%    \end{center}
    \printbibliography[heading=none]
\end{frame}
}

\end{document}